FROM debian:10.13-slim AS build
LABEL maintainer="Edan Brooke"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y && \
    apt-get upgrade -y

RUN apt-get install -y \
    wget \
    gnupg2

COPY systemctl /usr/bin/systemctl
RUN chmod +x /usr/bin/systemctl

RUN wget -O- http://downloads.3cx.com/downloads/3cxpbx/public.key | apt-key add - && \
    echo "deb http://downloads.3cx.com/downloads/debian stretch main" | tee /etc/apt/sources.list.d/3cxpbx.list

RUN apt-get update -y

RUN groupadd tcxsbc --gid 998 && \
    useradd tcxsbc -d /var/lib/3cxsbc --uid 998 --gid 998

RUN apt-get install -y 3cxsbc


FROM debian:10.13-slim AS sbc
LABEL maintainer="Edan Brooke"

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y \
        dumb-init \
        openssl \
        sudo \
        wget

COPY systemctl /usr/bin/systemctl
RUN chmod +x /usr/bin/systemctl

RUN groupadd tcxsbc --gid 998 && \
    useradd tcxsbc -d /var/lib/3cxsbc --uid 998 --gid 998

COPY --from=build --chown=998:998 /var/lib/3cxsbc /var/lib/3cxsbc
COPY --from=build --chown=998:998 /usr/sbin/3cxsbc /usr/sbin/3cxsbc
COPY --from=build --chown=998:998 /usr/sbin/3cxsbc-auto-update /usr/sbin/3cxsbc-auto-update
COPY --from=build --chown=998:998 /usr/sbin/3cxsbc-reprovision /usr/sbin/3cxsbc-reprovision

RUN mkdir -p /tmp && touch /etc/3cxsbc.conf.local && chown 998:998 /etc/3cxsbc.conf.local

EXPOSE 5060/tcp
EXPOSE 5090/tcp

ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]
CMD [ "/usr/sbin/3cxsbc", "-m", "/etc/3cxsbc.conf" ]
