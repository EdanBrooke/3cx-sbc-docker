# 3CX Session Border Controller (SBC) in Docker
This is not intended for production use and is a proof of concept. This is available as-is and comes with no warranty or support.

As 3CX uses `LocalSipAddr` for the bind address as well as the address that phones point to for provisioning the container must use the `macvlan` network driver so your handset(s) can access the container.

```
# Build Docker image
docker build . -t 3cxsbc
# Create macvlan network (change subnet and gateway to match your network)
docker network create -d macvlan --subnet 192.168.0.0/24 --gateway 192.168.0.1 -o parent=en0 3cx-sbc-net
# Start the Docker container
docker run --rm -it -v $(pwd)/3cxsbc.conf:/etc/3cxsbc.conf:ro -p 5060:5060 -p 5090:5090 --network 3cx-sbc-net 3cxsbc
```
